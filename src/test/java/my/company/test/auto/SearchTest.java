package my.company.test.auto;

import com.codeborne.selenide.conditions.ExactText;
import io.qameta.allure.AllureId;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.junit4.DisplayName;
import my.company.core.Features;
import my.company.core.Owners;
import my.company.pages.OnePage;
import my.company.pages.SearchResultsPage;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.open;

public class SearchTest {

    @Test
    @Feature(Features.SEARCH)
    @Owner(Owners.VIKTOR_M)
    @DisplayName("При точном вхождении поискового запроса переход осуществляется сразу на страницу")
    @AllureId("4")
    public void userCanSearch() {
        open("https://ru.wikipedia.org/wiki/");
        new OnePage().searchFor("java");
        SearchResultsPage results = new SearchResultsPage();
        results.getHeaderText().shouldHave(new ExactText("java"));
    }

}
