package my.company.test.manual;

import io.qameta.allure.AllureId;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.junit4.DisplayName;
import my.company.core.Features;
import my.company.core.Owners;
import my.company.test.BaseTest;
import org.junit.Test;


public class MarkupTest extends BaseTest {
    @Test
    @Feature(Features.SEARCH)
    @Owner(Owners.VIKTOR_M)
    @DisplayName("Поиск с главной: Проверка верстки переход на страницу по нажатию Enter")
    @AllureId("2")
    public void searchonMainDropDown() {
        step("Открыть страницу https://ru.wikipedia.org/wiki/");
        step("Проверить наличие строки поиска и кнопки искать");
        substep("Проверить, что переход успешен",
                "Ввести точное вхождение фразы, например 'java'",
                "Нажать Enter"
        );
        step("Проверить, что мы попали на https://ru.wikipedia.org/wiki/java");
    }

}
