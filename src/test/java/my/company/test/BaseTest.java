package my.company.test;

import io.qameta.allure.Allure;

import java.util.List;

public class BaseTest {
    public void step(String desc) {
        Allure.step(desc);
        System.out.println(desc);
    }

    public void substep(String step, List<String> substps) {
        Allure.step(step, (f) -> {
            substps.forEach(Allure::step);
        });
    }

    public void substep(String step, String... substeps) {
        Allure.step(step, (f) -> {
            for (String st : substeps) {
                Allure.step(st);
            }
        });
    }

}
