package my.company.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class WikiPage {
    private SelenideElement searchField = $(byId("searchInput"));
    private SelenideElement searchBtn = $(byId("searchButton"));
    private ElementsCollection suggests = $$(byClassName("mw-searchSuggest-link"));

    public void searchFor(String text) {
        searchField.setValue(text);
        searchBtn.click();
    }

    public ElementsCollection getSuggests() {
        return suggests;
    }


    public WikiPage suggest(String text) {
        searchField.type(text);
        return this;
    }
}
