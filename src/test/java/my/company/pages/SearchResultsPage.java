package my.company.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selenide.$;

public class SearchResultsPage extends WikiPage {
    SelenideElement header = $(byClassName("mw-page-title-main"));
    public SelenideElement getHeaderText() {
        return header;
    }

}
