package my.company.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

public class OnePage extends  WikiPage{
    private SelenideElement searchField = $(byId("searchInput"));
    private SelenideElement searchBtn = $(byId("searchButton"));

    public void searchFor(String text) {
        searchField.setValue(text);
        searchBtn.click();
    }
}
