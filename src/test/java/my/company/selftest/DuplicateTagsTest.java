package my.company.selftest;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class DuplicateTagsTest {
    /**
     * Ищет дубли @AllureId по тестам
     */
    @Test
    public void duplicateTag() {
//        сигнатура для поиска ID
        final String s = "@AllureId(";
        List<String> folders = Helper.getControlledFolders();
        Set<String> ids = ConcurrentHashMap.newKeySet();
        Set<String> duplicates = ConcurrentHashMap.newKeySet();
        AtomicInteger count = new AtomicInteger();
        for (String folder : folders) {
//            для всех файлов что нашли
            try (Stream<Path> paths = Files.walk(Paths.get(folder))) {
                paths
//                        Выбираем те, что RegularFile
                        .filter(Files::isRegularFile)
                        .forEach(e -> {
                            try (Stream<String> linesInt = Files.lines(e)) {
                                //ищем сигнатуру тега
                                linesInt.filter(line -> line.contains(s))
                                        .forEach(f -> {
                                            //чистим от паразитных пробелов
                                            f = f.replaceAll(" ", "");
                                            //увеличиваем счетчик ид
                                            count.getAndIncrement();
                                            //помещаем тег в сет
                                            ids.add(f);
                                            //если на этом этапе у нас размеры двух счтетчиков не совпали, то у нас дубль
                                            if (count.get() > ids.size() + duplicates.size()) {
                                                duplicates.add(f);
                                                System.err.println("В тестах найдены дубликаты ID:" + f);
                                            }
                                        });
                            } catch (IOException exception) {
                                System.err.println(exception.getMessage());
                            }
                        });
            } catch (IOException exception) {
                System.err.println(exception.getMessage());
            }
        }
        if (duplicates.size() != 0) {
            throw new AssertionError("В тестах найдены дубликаты ID:" + duplicates.toString());
        }
    }


}
