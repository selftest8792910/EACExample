package my.company.selftest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Helper {
    /**
     * @return список папок для авто-контроля
     */
    public static List<String> getControlledFolders() {
        java.io.File file = new java.io.File("");
        String abs = file.getAbsolutePath() + "/src/test/java/";
        List<String> folders = new ArrayList<>();
//       здесь можно перечислить паки, подлежащие контролю
        Collections.addAll(folders,
                abs + "my/company/test"
        );
        return folders;
    }
}
