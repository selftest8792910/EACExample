package my.company.selftest;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MarkupTest {
    /**
     * Прверяет что в папках контрля
     * 1. Есть Features
     * 2. Есть Owners
     * 3. Они из списка
     * <p>
     * Тест работает так: если для метода поставили @Test то на каждый такой тест должно быть по N тегов разметки
     */
    @Test
    public void checkMarkup() {
        List<String> controlledTags = new ArrayList<>();
//      Здесь перечислены обязательные теги
//      Если там будет что-то не из файлов Features/Owners то не скомпилириуется проект или тест упадет
        final String feature = "@Feature(Features.";
        final String owner = "@Owner(Owners.";
        final String allure = "@AllureId(";
        Collections.addAll(controlledTags, feature,
                owner,
                allure
        );
        final String test = "@Test";
        List<String> errors = new ArrayList<>();
        List<String> folders = Helper.getControlledFolders();
        for (String folder : folders) {
//            для всех файлов что нашли
            try (Stream<Path> paths = Files.walk(Paths.get(folder))) {
                paths
//                        .filter(filePath -> filePath.toString().toLowerCase().endsWith("test.java"))
                        //для всех файлов тестов
                        .filter(Files::isRegularFile)
                        .forEach(e -> {
                            AtomicInteger tagUsed = new AtomicInteger(0);
                            AtomicInteger testUsed = new AtomicInteger(0);
                            try (Stream<String> linesInt = Files.lines(e)) {
                                List<String> stringList = linesInt.collect(Collectors.toList());
                                //зачитываем все теги
                                stringList.stream().filter(line -> line.contains("@")).forEach(
                                        f -> {
                                            f = f.replaceAll(" ", "");
                                            //проверяем контролируемые теги
                                            for (String ctag : controlledTags) {
                                                if (f.contains(ctag)) {
                                                    tagUsed.getAndIncrement();
                                                }
                                            }
                                            //и считаем число тестов в файле
                                            if (f.contains(test)) {
                                                testUsed.getAndAdd(controlledTags.size());
                                            }
                                        }
                                );
                            } catch (IOException exception) {
                                System.err.println(exception + e.toString());
                            }
                            if (tagUsed.get() != testUsed.get()) {
                                System.err.println("В файле нет обязательных тегов " + e.getFileName().toString());
                                errors.add(e.getFileName().toString());
                            }

                        });
            } catch (IOException exception) {
                exception.printStackTrace();
            }

        }
        if (errors.size() != 0) {
            throw new AssertionError("В файлах нет обязательных тегов: " + errors.toString());
        }

    }
}
